olap4j (1.3.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Building with Maven, adding Maven rules
  * Skipping tests, that are relying on newer mondrian
  * Calling javacc and filling in a template Java file from d/rules
  * Using javahelper to fill in the classpath and generate dependencies
  * Changing the install path of the Javadoc
  * Adding a Lintian override for embedded JS
  * Expliciting Rules-Requires-Root: no
  * Marking the -doc package as Multi-Arch: foreign

  [ Andreas Tille ]
  * Point Homepage to Github
  * Fix watch file
  * d/copyright: review (thanks to lrc)
  * Install NOTICE file
  * Fix Vcs fields
  * Switch from cdbs to dh
  * debhelper-compat = 13
  * Exclude files via Files-Excluded
  * Standards-Version: 4.7.0 (routine-update)
  * Delete debian/README.source
  * New upstream version
  * New upstream version 1.3.0+dfsg
  * Try building new upstream version but failed

 -- Pierre Gruet <pgt@debian.org>  Fri, 31 Jan 2025 16:50:16 +0100

olap4j (1.2.0-2) unstable; urgency=medium

  * Team upload.

  [ Chris West (Faux) ]
  * Fix build with Java 9 by fiddling generics. (Closes: #873216)

  [ Markus Koschany ]
  * Switch to compat level 10.
  * Use canonical VCS address.
  * d/copyright: Use https for Format field.
  * Declare compliance with Debian Policy 4.1.2.
  * Build for Java 7.

 -- Markus Koschany <apo@debian.org>  Tue, 26 Dec 2017 21:23:40 +0100

olap4j (1.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - Refreshed the patches
    - Replaced the build dependency on cup with javacc
    - Removed the dependency on liblog4j1.2-java
  * Install the Maven artifacts
  * Improved the reproducibility:
    - Set the locale when generating the javadoc and remove the timestamps)
    - Removed VERSION.txt from the jar files (contains timestamps, usernames)
  * debian/control:
    - Moved the dependency on libcommons-dbcp-java to Suggests
    - Standards-Version updated to 3.9.6 (no changes)
  * Use XZ compression for the upstream tarball
  * Moved the package to Git
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 21 Oct 2015 11:17:12 +0200

olap4j (1.0.1.500-2) unstable; urgency=low

  * Team upload.
  * Compatible with java7: unmappable character for encoding ASCII
    (Closes: #717258)

  [ Sylvestre Ledru ]
  * Change the maintainer for the Java team.
  * Standards-Version updated to 3.9.4

 -- ShuxiongYe <yeshuxiong@gmail.com>  Fri, 19 Jul 2013 03:39:47 +0800

olap4j (1.0.1.500-1) unstable; urgency=low

  * New upstream release:
    - Fix compatibility with JDBC 4.1 (Java 7).
  * Switch to 3.0 (quilt) source format.
  * d/control: Bump Standards-Version to 3.9.3: no changes needed.
  * d/control: Remove JRE dependency for library package.
  * Remove deprecated OLAP4J Specification from -doc package.
  * d/copyright: Upgrade to lastest copyright format 1.0

 -- Damien Raude-Morvan <drazzib@debian.org>  Tue, 26 Jun 2012 23:01:28 +0200

olap4j (0.9.8~svn293-1) unstable; urgency=low

  * Initial release. (Closes: #560236)

 -- Damien Raude-Morvan <drazzib@debian.org>  Sun, 13 Dec 2009 17:51:53 +0100
